CS442 Design Patterns
Fall 2016
ASSIGNMENT 5 README FILE

Due Date: Friday, December 16, 2016
Submission Date: Friday, December 16, 2016
Grace Period Used This Project: 0
Grace Period Remaining: 0
Author(s): Mariuxi Yagual
e-mail(s): myagual1@binghamton.edu


PURPOSE:

[
 	Use reflection to create instances of objects First and Second using data in input file. To store all
 	the objects I used an Arraylist because it can grow or shrink its size dynamically and the overall complexity will be equivalent to O(1)
 	for inserting and indexing. Besides Arraylist contains many useful methods that makes it simple to manipulate the 
 	data stored. I used another Arraylist to separate First and Second instances plus an HashSet to  implement my hashcode implementation and easily 
 	get the unique values. HashSets are very useful because they use a mechanism called hashing and the transformation of the key into its hash code is performed automatically
 	and it's time complexity is O(1) which is more efficient than comparing and iterating through the list to find duplicates.
]

PERCENT COMPLETE:

[
  I believe I have completed 100% of this project in all its entirety 
]

PARTS THAT ARE NOT COMPLETE:

[
  All parts are complete. I did not implement the logger fully.
]

BUGS:

[
 None
]

FILES:

[
  Included with this project are 8 files
  README, the text file you are presently reading
]

SAMPLE OUTPUT:

[
  Mariuxis-MacBook-Air:wordCount mariuxiyagual$ ant -Dargs1=genericDeser/input.txt -Dargs2=0 run
Buildfile: /Users/mariuxiyagual/Desktop/cs442/Yagual_Mariuxi_assign5/genericDeser/build.xml

jar:
     [jar] Building jar: /Users/mariuxiyagual/Desktop/cs442/Yagual_Mariuxi_assign5/genericDeser/BUILD/jar/genericDeser.jar

run:
     [java] Number of unique First objects: 4952
     [java] Total Number of First objects: 4952
     [java] Number of unique Second objects: 5018
     [java] Total Number of Second objects: 5048

BUILD SUCCESSFUL
]

TO COMPILE:

[
  Extract the files and then in the terminal type "ant"
]

TO RUN:

[
  In the terminal, please run as: "ant -Dargs1=genericDeser/input.txt -Dargs2=0 run"
  The values after the equal sign are the arguments, please enter your own 
]

EXTRA CREDIT:

[
  N/A
]


BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.

[
	-http://stackoverflow.com/questions/8694984/remove-part-of-string
	-http://stackoverflow.com/questions/13429119/get-unique-values-from-arraylist-in-java

]

ACKNOWLEDGEMENT:

[
  During the coding process no classmates were consulted.

]