package genericDeser.driver;
import genericDeser.util.*;
import java.io.FileNotFoundException;

public class Driver{
	public static void main(String[] args){
			if(args.length < 2){
				System.out.println("Please enter two arguments");
				return;
			}
			
			String input = args[0];
			int DEBUG_LEVEL = Integer.parseInt(args[1]);
			PopulateObjects po = new PopulateObjects(input);
			po.deserObjects();
			po.printResults();
			
			return;
	}
}