package genericDeser.util;

/**
 * Java class
 * @author mariuxiyagual
 *
 */
public class Second{
	private double DoubleValue;
	private int IntValue;
	private boolean BooleanValue;
	
	/**
	 * empty constructor
	 */
	public Second(){}
	
	/**
	 * setters and getters
	 */
	public void setIntValue(int iIn){
		IntValue = iIn;
	}
	
	public void setDoubleValue(double dIn){
		DoubleValue = dIn;
	}
	
	public void setBooleanValue(boolean bIn){
		BooleanValue = bIn;
	//	System.out.println("test." + BooleanValue +  getDoubleValue() + " " + IntValue);
	}
	
	public int getIntValue(){
		return IntValue;
	}
	
	public double getDoubleValue(){
		return DoubleValue;
	}
	
	public boolean getBooleanValue(){
		return BooleanValue;
	}

	/**
	 * override of equals, used in hashset
	 */
	@Override public boolean equals(Object obj){
		boolean rval = false;
		if(obj instanceof Second){
			Second scd = (Second)obj;
			rval = (scd.getIntValue() == this.getIntValue()) &&
					(scd.getDoubleValue() == this.getDoubleValue()) &&
					(scd.getBooleanValue() == this.getBooleanValue());
		}
		return rval;
	}
	
	/**
	 * override of hashCode
	 */
	@Override public int hashCode(){
		int rval = (this.getIntValue() + (int)this.getDoubleValue()) * 31;
		return rval;
	}
	
	/**
	 * toString method
	 */
	@Override public String toString(){
		return "Second"+ " "+ getIntValue();
	}
}