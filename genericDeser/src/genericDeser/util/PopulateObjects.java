package genericDeser.util;
import java.lang.Class;
import java.util.ArrayList;
import java.lang.reflect.Method;
import genericDeser.fileOperations.FileProcessor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Array;
import java.util.*;
import java.lang.String;

/**
 * Class that contains data structure to store instances of First and Second. 
 * @author mariuxiyagual
 *
 */
public class PopulateObjects{
	private ArrayList<Object> ar;
	private String input;
	
	/**
	 * constructor that saves the file name
	 */
	public PopulateObjects(String filename){
		ar = new ArrayList<Object>();
		input = filename;
	}
	
	/**
	 *  Reads data member values from an inputFile and accordingly create instances of First and Second.
	 */
	public void deserObjects(){
		try{
			FileProcessor fp = new FileProcessor();
			fp.openReader(input);
			String[] line;
			String classStr = null;
			Class cls = null;
			Class[] signature = new Class[1];
			Object[] params = new Object[1];
			Object obj = null;
			Object rval = null;
			int ret = 0;
			while((line = fp.getStringArray()) != null){
				if(line[0].contains("fqn:")){
					classStr = line[0].substring(4);
					cls = Class.forName(classStr);
					obj = cls.newInstance();
				}
				else if(cls != null && obj != null){
					int startindex = line[1].indexOf('=');
					int endindex = line[1].indexOf(',');
					String mname = "set" + line[1].substring(startindex+1, endindex);
					//eliminate ,
					
					if(classStr.compareTo("genericDeser.util.First") == 0){
						if(line[0].contains("type=int")){
							int p = Integer.parseInt(line[2].substring(6));
							signature[0] = Integer.TYPE;
							params[0] = new Integer(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else if(line[0].contains("type=float")){
							float p = Float.parseFloat(line[2].substring(6));
							signature[0] = Float.TYPE;
							params[0] = new Float(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else if(line[0].contains("type=short")){
							short p = Short.parseShort(line[2].substring(6));
							signature[0] = Short.TYPE;
							params[0] = new Short(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else if(line[0].contains("type=String")){
							String p = line[2].substring(6);
							signature[0] = String.class;
							params[0] = new String(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else{
							System.out.println("Unidentified type error");
							System.exit(0);
						}
						if(ret == 4){
							if(obj == null){
								System.out.println("rval is null for first");
								return;
							}
							First fi = (First)obj;
							ar.add(fi);
							ret = 0;
						}
					}
					else if(classStr.compareTo("genericDeser.util.Second") == 0){
						if(line[0].contains("type=int")){
							int p = Integer.parseInt(line[2].substring(6));
							signature[0] = Integer.TYPE;
							params[0] = new Integer(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else if(line[0].contains("type=double")){
							double p = Double.parseDouble(line[2].substring(6));
							signature[0] = Double.TYPE;
							params[0] = new Double(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else if(line[0].contains("type=boolean")){
							boolean p = Boolean.parseBoolean(line[2].substring(6));
							signature[0] = Boolean.TYPE;
							params[0] = new Boolean(p);
							invokeMethod(obj, cls, signature, mname, params);
							ret++;
						}
						else{
							System.out.println("Unidentified type error");
							System.exit(0);
						}
						if(ret == 3){
							if(obj == null){
								System.out.println("rval is null");
								return;
							}
							Second sec = (Second)obj;
							ar.add(sec);
							ret = 0;
						}
					}
					else{
						System.out.println("class is null error");
					}
				}
			}
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			System.exit(0);
		}
		catch(InstantiationException ex1){
			ex1.printStackTrace();
			System.exit(0);
		}catch(IllegalAccessException ex2){
			ex2.printStackTrace();
			System.exit(0);
		}
		//to read data member values from an inputFile
		//and accordingly create instances of First and Second. 
		//Decide the appropriate return value and parameters for the method deserObjects
	}

	/**
	 * Method used for reflection method calling
	 */
	private void invokeMethod(Object obj, Class cls, Class[] signature, String mname, Object[] params){
		try{
			//String mdname = "setIntValue";
			Method meth = cls.getMethod(mname, signature);
			Object result = meth.invoke(obj, params);
		}catch(IllegalAccessException ex2){
			ex2.printStackTrace();
			System.exit(0);
		}catch(NoSuchMethodException ex3){
			ex3.printStackTrace();
			System.exit(0);
		}catch(InvocationTargetException ex4){
			ex4.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * Method returns the number of non-duplicate instances of First 
	 */
	private long getTotalFirst(){
		long instances = 0;
		ArrayList<First> fir = new ArrayList<First>();
		Set<First> far = null;
		try{
			Class cls = Class.forName("genericDeser.util.First");
			for(Object o : ar){
				if(o.getClass() == cls){
					fir.add((First)o);	
				}	
			}
			far = new HashSet<First>(fir);
			System.out.println("Number of unique First objects: " + far.size());
			System.out.println("Total Number of First objects: " + fir.size());
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			System.exit(0);
		}
		return instances;
	}
	
	/**
	 *  return the number of non-duplicate instances of Second
	 */
	private long getTotalSecond(){
		long instances = 0;
		ArrayList<Second> fir = new ArrayList<Second>();
		Set<Second> far = null;
		try{
			Class cls = Class.forName("genericDeser.util.Second");
			for(Object o : ar){
				if(o.getClass() == cls){
					fir.add((Second)o);	
				}	
			}
			far = new HashSet<Second>(fir);
			System.out.println("Number of unique Second objects: " + far.size());
			System.out.println("Total Number of Second objects: " + fir.size());
		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
			System.exit(0);
		}
		return instances;
	}
	
	/**
	 * prints results of total number and non-duplicates
	 */
	public void printResults(){
		getTotalFirst();
		getTotalSecond();
	}
	
	/**
	 * prints array of all objects
	 */
	public void printArraylist(){
		for(Object o : ar){
			System.out.println("element in ar: " + o.toString());
		}
	}
}