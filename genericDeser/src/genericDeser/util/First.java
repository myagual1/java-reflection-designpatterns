package genericDeser.util;

import java.util.Set;
import java.lang.Object;

/**
 * Java class
 * @author mariuxiyagual
 *
 */
public class First{
	private int IntValue;
	private String StringValue;
	private float FloatValue;
	private short ShortValue;
	
	/**
	 * empty constructor
	 */
	public First(){
		
	}
	
	/**
	 * setters and getters
	 */
	public void setIntValue(int iIn) {
		IntValue = iIn;
	}
	public void setStringValue(String sIn) {
		StringValue = sIn;
		//System.out.println("test." + StringValue + " " + IntValue);
	}
	public void setFloatValue(float fIn) {
		FloatValue = fIn;
		//System.out.println("test." + FloatValue);
	}
	public void setShortValue(short shIn){
		 ShortValue = shIn;
		//System.out.println("test." + ShortValue);
	}
	
	public int getIntValue() {
		return IntValue;
	}
	public String getStringValue() {
		return StringValue;
	}
	public float getFloatValue() {
		return FloatValue;
	}
	public short getShortValue(){
		 return ShortValue;
	}
	
	/**
	 * override of equals, used in hashset
	 */
	@Override public boolean equals(Object obj){
		boolean rval = false;
		if(obj instanceof First){
			First scd = (First)obj;
			rval = (scd.getIntValue() == this.getIntValue()) &&
					(scd.getStringValue() == this.getStringValue()) &&
					(scd.getFloatValue() == this.getFloatValue()) &&
					(scd.getShortValue() == this.getShortValue());
		}
		return rval;
	}
	
	/**
	 * override of hashCode
	 */
	@Override public int hashCode(){
		int rval = (this.getIntValue() + (int)this.getFloatValue() + (int)this.getShortValue()) * 31;
		return rval;
	}
	
	/**
	 * toString method
	 */
	@Override public String toString(){
		return "First " + this.getStringValue();
	}
}